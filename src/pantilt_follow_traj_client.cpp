#include <ros/ros.h>
#include <std_srvs/Empty.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pantilt_follow_traj_client");
    ros::NodeHandle nh;

    // client1 calls the set_trajectory service
    ros::ServiceClient client1 = nh.serviceClient<std_srvs::Empty>("set_trajectory");
    std_srvs::Empty::Request req1;
    std_srvs::Empty::Response resp1;
    ros::service::waitForService("set_trajectory", ros::Duration(5));
    bool success1 = client1.call(req1, resp1);

    if (success1) {
        ROS_INFO_STREAM("SERVICE 1 SUCCEED");
    } else {
        ROS_ERROR_STREAM("SERVICE 1 FAILED");
    }

    // client2 calls the set_tolerance service
    ros::ServiceClient client2 = nh.serviceClient<std_srvs::Empty>("set_tolerance");
    std_srvs::Empty::Request req2;
    std_srvs::Empty::Response resp2;
    ros::service::waitForService("set_tolerance", ros::Duration(5));
    bool success2 = client2.call(req2, resp2);

    if (success2) {
        ROS_INFO_STREAM("SERVICE 2 SUCCEED");
    } else {
        ROS_ERROR_STREAM("SERVICE 2 FAILED");
    }

    // client3 calls the send_goal service
    ros::ServiceClient client3 = nh.serviceClient<std_srvs::Empty>("send_goal");
    std_srvs::Empty::Request req3;
    std_srvs::Empty::Response resp3;
    ros::service::waitForService("send_goal", ros::Duration(5));
    bool success3 = client3.call(req3, resp3);

    if (success3) {
        ROS_INFO_STREAM("SERVICE 3 SUCCEED");
    } else {
        ROS_ERROR_STREAM("SERVICE 3 FAILED");
    }
    return 1;
}
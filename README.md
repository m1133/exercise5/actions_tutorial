Session 7: Communications using actions (https://sir.upc.edu/projects/rostutorials)

Exercise 5a:
1. Create a node called follow_traj_wrap_server in a follow_traj_wrap_server.cpp file, by modifying the pantilt_follow_traj.cpp file so that the node offers different services that encapsulate the main functions involved in the control of the robot using the FollowJointTrajectory action, i.e. services to:
    - set a trajectory,
    - define the tolerance parameters and
    - send the goal to move the robot along the trajectory.
2. Then implement the node pantilt_follow_traj_client in a file called pantilt_follow_traj_client.cpp in order to use these services to control the pan-tilt.
3. Use a launch file called pantilt_trajectory_controller_server.launch to start all the nodes.
